<?php
/**
 * Created by PhpStorm.
 * User: khairul
 * Date: 12/30/15
 * Time: 12:58 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function customer(){
        return $this->belongsTo('App\Customer');
    }

}