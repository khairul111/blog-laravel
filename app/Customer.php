<?php
/**
 * Created by PhpStorm.
 * User: khairul
 * Date: 12/30/15
 * Time: 12:56 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model{

    public function orders(){
        return $this->hasMany('App\Order');
    }
}